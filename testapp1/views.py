# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Document
from .forms import DocumentForm, DeleteDocumentForm


def list(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            col1 = request.POST.get('col1')[1:]
            col2 = request.POST.get('col2')[1:]
            newdoc = Document(docfile=request.FILES['docfile'], col1=col1, col2=col2)
            newdoc.save()

            return HttpResponseRedirect(reverse('list'))
    else:
        form = DocumentForm()

    documents = Document.objects.all()

    return render(
        request,
        'list.html',
        {'documents': documents, 'form': form}
    )

def details(request, id):
    document = get_object_or_404(Document, id=id)
    template_vars = {'document': document}
    return render(request, 'details.html', template_vars)

def delete(request, id):
    document = get_object_or_404(Document, id=id)

    if request.method == 'POST':
        form = DeleteDocumentForm(request.POST, instance=document)

        if form.is_valid():
            document.delete()
            return HttpResponseRedirect(reverse('list'))

    else:
        form = DeleteDocumentForm(instance=document)

    template_vars = {'form': form}
    return render(request, 'delete.html', template_vars)
