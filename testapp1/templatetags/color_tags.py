from django import template
import urllib, hashlib

from ..utils import color_range

register = template.Library()

@register.tag
def color_range_tag(col1web, col2web):
    return color_range(col1web, col2web)
