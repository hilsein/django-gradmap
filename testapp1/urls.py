from django.conf.urls import url
from .views import list, delete, details

urlpatterns = [
    url(r'^$', list, name='list'),
    url(r'^/(?P<id>[0-9]+)/$', details, name='details'),
    url(r'^/(?P<id>[0-9]+)/delete/$', delete, name='delete'),
]
