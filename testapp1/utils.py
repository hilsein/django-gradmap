from colour import Color
import numpy as np
import cv2

def color_range(col1web, col2web):
    if col1web and col2web:
        col1 = Color(col1web)
        col2 = Color(col2web)
        return list(col1.range_to(col2, 256))
    else:
        return []

def apply_colormap(im_gray, col1web, col2web) :

    lut = np.zeros((256, 1, 3), dtype=np.uint8)
    ind = 0
    for c in color_range(col1web, col2web):
        ch = c.hex
        ch = ch[1:]
        if len(ch) == 6:
            r = int(ch[0:2], 16)
            g = int(ch[2:4], 16)
            b = int(ch[4:6], 16)
        elif len(ch) == 3:
            r = int('0' + ch[0], 16)
            g = int('0' + ch[1], 16)
            b = int('0' + ch[2], 16)

        lut[:, 0, 0][ind] = b
        lut[:, 0, 1][ind] = g
        lut[:, 0, 2][ind] = r
        
        ind = ind + 1

    im_color = cv2.LUT(im_gray, lut)
    
    return im_color;

# via matplotlib
#mycm=matplotlib.colors.LinearSegmentedColormap.from_list('',['#582f91', '#00aeef'])
#imgrad=mycm(image[:,:,0])

