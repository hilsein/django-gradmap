# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

from django.db.models.signals import post_save
from django.dispatch import receiver

from django.core.files.base import ContentFile
from django.core.files import File

from PIL import Image, ImageOps
from colour import Color
import os
import uuid
import cv2

from colorfield.fields import ColorField

from .utils import color_range, apply_colormap

class Document(models.Model):
    docfile = models.ImageField(upload_to='documents/%Y/%m/%d')
    gray = models.ImageField(upload_to='gray/%Y/%m/%d')
    col1 = ColorField(default="582f91") 
    col2 = ColorField(default="00aeef") 
    graymap = models.ImageField(upload_to='graymap/%Y/%m/%d')
    thumb = models.ImageField(upload_to='thumbs/%Y/%m/%d')

    def save(self, *args, **kwargs):
        super(Document, self).save(*args, **kwargs)

    @property
    def col1web(self):
        return '#' + self.col1

    @property
    def col2web(self):
        return '#' + self.col2

    @property
    def color_range(self):
        return color_range('#'+self.col1, '#'+self.col2)

    def prep_paths_for(self, name):
        _dir = os.path.join(settings.MEDIA_ROOT, name)
        try: 
            os.makedirs(_dir)
        except OSError:
            if not os.path.isdir(_dir):
                raise
        basename = os.path.basename(self.docfile.path)
        path = os.path.join(_dir, basename)
        return (basename, path)
    
    def mk_thumb(self):
        basename, path = self.prep_paths_for('thumbs')

        thumb_file = Image.open(self.docfile.path)
        if thumb_file.mode not in ("L", "RGB"):
            thumb_file = thumb_file.convert("RGB")
        thumb_image_fit = ImageOps.fit(thumb_file, (100, 100), Image.ANTIALIAS)
        thumb_image_fit.save(path)
        temp_data = open(path, "r")
        image_file = File(temp_data)
        if self.thumb != image_file:
            self.thumb.save(basename, image_file)

    def mk_gray(self):
        basename, path = self.prep_paths_for('gray')
    
        image = cv2.imread(self.docfile.path)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
        cv2.imwrite(path, gray_image)
        temp_data = open(path, "r")
        image_file = File(temp_data)
        if self.gray != image_file:
            self.gray.save(basename, image_file)

    def mk_graymap(self):
        basename, path = self.prep_paths_for('graymap')
    
        im = cv2.imread(self.docfile.path, cv2.IMREAD_GRAYSCALE);
        im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR);
        im_color = apply_colormap(im, '#'+self.col1, '#'+self.col2);
    
        cv2.imwrite(path, im_color)
        temp_data = open(path, "r")
        image_file = File(temp_data)
        if self.graymap != image_file:
            self.graymap.save(basename, image_file)


__postSaveDocumentLock = {}
@receiver(post_save, sender=Document, dispatch_uid=str(uuid.uuid1()))
def post_save_document(sender, **kwargs):
    document = kwargs.get('instance')
    if document.pk in __postSaveDocumentLock:
        return
    else:
        __postSaveDocumentLock[document.pk] = True
    document.mk_thumb()
    document.mk_gray()
    document.mk_graymap()
    if document.pk in __postSaveDocumentLock:
        del __postSaveDocumentLock[document.pk]

